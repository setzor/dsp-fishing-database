-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.20-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5244
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table dspdb.char_vars
CREATE TABLE IF NOT EXISTS `char_vars` (
  `charid` int(10) unsigned NOT NULL,
  `varname` varchar(30) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`charid`,`varname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table dspdb.char_vars: 22 rows
/*!40000 ALTER TABLE `char_vars` DISABLE KEYS */;
INSERT INTO `char_vars` (`charid`, `varname`, `value`) VALUES
	(21828, 'Guild_Member', 32),
	(21828, 'ridingOnTheClouds_1', 2),
	(21829, 'MoghouseExplication', 1),
	(21829, 'fov_numkilled1', 5),
	(21829, 'fov_repeat', 1),
	(21829, 'fov_numneeded1', 6),
	(21829, 'fov_regimeid', 27),
	(21829, 'CONQUEST_RING_TIMER', 1511190000),
	(21829, 'fov_LastReward', 464020),
	(21828, 'ridingOnTheClouds_2', 2),
	(21828, 'ridingOnTheClouds_3', 6),
	(21828, 'ridingOnTheClouds_4', 2),
	(21828, 'trade_itemid', 1131),
	(21828, 'bcnm_instanceid', 1),
	(21828, 'TokakaSpokenTo', 1),
	(21828, 'trade_bcnmid', 77),
	(21828, 'maatsCap', 1),
	(21828, 'DynamisID', 1514001880),
	(21828, 'dynaWaitxDay', 1513954800),
	(21830, 'MoghouseExplication', 1),
	(21828, 'GodMode', 0),
	(21829, 'GodMode', 0);
/*!40000 ALTER TABLE `char_vars` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
